import { ConsignacionDTO } from '../sections/pagos/ConsignacionDTO';

export interface RegistrarPagosConsignacionDTO {
    consignacion: ConsignacionDTO[];
}
