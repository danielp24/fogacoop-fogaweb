import { PsdPortal } from '../sections/psd/PsdPortal';
import { PagoPsdPortalDTO } from '../sections/psd/PagoPsdPortalDTO';

export interface PagoPortalDTO {

    psdPortal: PsdPortal[];
    pagoPortal: PagoPsdPortalDTO[];

}
