import { CooperativaDTO } from '../sections/cooperativa/CooperativaDTO';
import { ConsultaMovimientosDTO } from '../sections/cooperativa/ConsultaMovimientosDTO';

export interface ConsultaMovimientosCooperativaDTO {

     cooperativa: CooperativaDTO[];
     consultaMovimientos: ConsultaMovimientosDTO[];

}
