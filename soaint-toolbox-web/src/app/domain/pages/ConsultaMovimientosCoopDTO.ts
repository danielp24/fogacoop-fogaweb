import { CooperativaDTO } from '../sections/cooperativa/CooperativaDTO';
import { ContactoDTO } from '../sections/cooperativa/ContactoDTO';
import { ConsultaMovimientosDTO } from '../sections/cooperativa/ConsultaMovimientosDTO';
import { CriteriosBusquedadDTO } from '../sections/cooperativa/CriteriosBusquedadDTO';

export interface ConsultaMovimientosCoopDTO {

    busquedad: CriteriosBusquedadDTO[];
    cooperativa: CooperativaDTO[];
    contacto: ContactoDTO[];
    movimientosCooperativa: ConsultaMovimientosDTO[];
}
