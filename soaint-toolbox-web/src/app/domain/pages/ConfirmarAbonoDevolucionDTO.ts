import {CooperativaDTO} from '../sections/cooperativa/CooperativaDTO';
import {ContactoDTO} from '../sections/cooperativa/ContactoDTO';
import {OperacionRealizadaDTO} from '../sections/operaciones/OperacionRealizadaDTO';
import {ProcessRequestDTO} from "../bpm/request/ProcessRequestDTO";

export interface ConfirmarAbonoDevolucionDTO {
  cooperativa: CooperativaDTO[];
  contacto: ContactoDTO[];
  operacionRealizada: OperacionRealizadaDTO[];

}

