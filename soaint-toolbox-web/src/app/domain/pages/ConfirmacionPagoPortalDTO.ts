import { ConfirmacionPagoDTO } from '../sections/pagos/ConfirmacionPagoDTO';

export interface ConfirmacionPagoPortalDTO {

    confirmacionPago: ConfirmacionPagoDTO[];

}
