import { CooperativaDTO } from '../sections/cooperativa/CooperativaDTO';
import { ContactoDTO } from '../sections/cooperativa/ContactoDTO';
import { PagoPsdDTO } from '../sections/psd/PagoPsdDTO';
import { GestionNovedadDTO } from '../sections/gestiones/GestionNovedadDTO';
import { TablaResumenDTO } from '../sections/tables/tablaResumenDTO';
import { LiquidacionPsdDTO } from '../sections/tables/LiquidacionPsdDTO';
import { TablaFechaPrimaDTO } from '../sections/tables/tablaFechaPrimaDTO';
import { TablaFechaNovedadDTO } from '../sections/tables/tablaFechaNovedadDTO';
import { TablaFechaPagoDTO } from '../sections/tables/tablaFechaPagoDTO';

export interface GestionarPagosPDSDTO {

    informacionCooperativa: CooperativaDTO[];
    contacto: ContactoDTO[];
    tablaResumen: TablaResumenDTO[];
    liquidacion: LiquidacionPsdDTO[];
    tablaFechaPrima: TablaFechaPrimaDTO[];
    tablaFechaNovedad: TablaFechaNovedadDTO[];
    pago: PagoPsdDTO[];
    tablafechaPago: TablaFechaPagoDTO[];
    gestionNovedad: GestionNovedadDTO[];

}
