export interface PagoNoLiquidadoDTO {

    NIT: string;
    cooperativa: string;
    fechaPago: string;
    valorPagado: number;
    estado: string;
    observaciones: string;

}
