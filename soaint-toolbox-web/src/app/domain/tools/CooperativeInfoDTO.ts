export class CooperativeInfoDTO {
  name: string;
  charge: string;
  email: string;
  departamento: string;
  city: string;
  indicative: number;
  conmutador: number;
  extension: number;
  fijo: number;
  celular: number;
  fecha: string;
}
