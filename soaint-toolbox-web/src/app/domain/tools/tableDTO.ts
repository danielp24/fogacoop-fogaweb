export interface Tab {
    fecha: string;
    desc: string;
    valor: string;
    saldoF: string;
    saldoPendiente: string;
}
