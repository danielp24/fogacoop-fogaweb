export class AutocompleteCooDTO {
    siaf: number;
    nombre: string;
    nit: number;
    tipoCooperativa: string;
}
