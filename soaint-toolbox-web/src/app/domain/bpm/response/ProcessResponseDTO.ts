import { ContenedorDTO } from "../general/ContenedorDTO";
import { ResponseDTO } from "../general/ResponseDTO";

export class ProcessResponseDTO {
  containers: Array<ContenedorDTO>
  response: ResponseDTO;
}
