import {OwnerUserDTO} from "../general/OwnerUserDTO";
import { ValuesDTO } from './ValuesDTO';

export class RequestParamsDTO {
  containerId: string;
  taskId: string;
  taskStatus: string;
  ownerUser: OwnerUserDTO;
  parametros ?: ValuesDTO;
}
