import { ParametrosDTO } from "../general/ParametrosDTO";
import { UserBPMDTO } from "../general/UserBPMDTO";

export class ProcessRequestDTO {
  containerId?: string;
  processesId?: string;
  processInstance?: string;
  taskId?: string;
  taskStatus?: string;
  groups?: string[];
  ownerUser?: UserBPMDTO;
  assignment?: UserBPMDTO;
  parametros?: ParametrosDTO;
  signal?: string;
}
