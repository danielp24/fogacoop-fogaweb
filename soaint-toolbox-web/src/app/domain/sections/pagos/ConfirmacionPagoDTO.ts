import { BancoDTO } from '../lists/BancoDTO';

export interface ConfirmacionPagoDTO {
    entidad: string;
    NIT: string;
    fechaPago: Date;
    estadoPago: string;
    numTransaccion: number;
    banco: BancoDTO[];
    valorPagado: number;
    descripcion: string;
}
