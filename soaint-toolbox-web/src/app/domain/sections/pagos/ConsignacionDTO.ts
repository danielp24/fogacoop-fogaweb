import { BancoDTO } from '../lists/BancoDTO';

export interface ConsignacionDTO {
    ciudad: string;
    fechaConsignacion: Date;
    banco: BancoDTO[];
    numTransferencia: number;
    NIT: string;
    nomCooperativa: string;
    valorPagado: number;
    observaciones: string;
}
