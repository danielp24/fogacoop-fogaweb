export interface CriteriosBusquedadDTO {

    nit: string;
    sigla: string;
    codSiaf: string;
    nombreCoop: string;

}
