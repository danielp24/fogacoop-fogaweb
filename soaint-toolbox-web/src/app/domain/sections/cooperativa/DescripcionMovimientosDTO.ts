export interface DescripcionMovimientosDTO {

    fecha: Date;
    descripcion: string;
    valor: number;
    saldoAfavor: number;
    saldoPendientePagar: number;
}
