import { DescripcionMovimientosDTO } from './DescripcionMovimientosDTO';

export interface ConsultaMovimientosDTO extends DescripcionMovimientosDTO {
    fechaInicio: Date;
    fechaFin: Date;
    primasPagadas: number;

}
