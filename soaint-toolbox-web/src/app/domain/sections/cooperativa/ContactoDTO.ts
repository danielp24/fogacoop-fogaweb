export interface ContactoDTO {

    nombreContact: string;
    cargoCont: string;
    correoElectronico: string;
    ciudadContac: string;
    indicativoContac: number;
    telefonosContact: number;
}
