export interface CooperativaDTO {

    codigoSiaf: number;
    nombreCoop: string;
    NIT: string;
    tipoCoop: string;
    sigla: string;
    fechaInscripcion: Date;
}
