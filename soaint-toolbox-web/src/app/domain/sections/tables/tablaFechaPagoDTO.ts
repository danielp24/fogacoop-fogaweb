export interface TablaFechaPagoDTO {
    fecha: string;
    valorPagado: number;
}
