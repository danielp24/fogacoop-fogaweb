export interface TablaResumenDTO {

    valorTotalPrima: number;
    valorPrimaNormal: number;
    valorPosicionNeta: number;
    valorSobrePrima: number;
    valorMora: number;
}
