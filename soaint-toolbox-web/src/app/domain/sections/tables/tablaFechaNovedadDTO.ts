export interface TablaFechaNovedadDTO {

    fechaInicial: string;
    fechaFinal: string;
    tipoNovedad: string;
    valorNovedad: number;
}
