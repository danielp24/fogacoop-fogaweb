import { BancoDTO } from '../lists/BancoDTO';
export interface PagoPsdPortalDTO {

    banco: BancoDTO[];
    nombreTitular: string;
    tipoPersona: string;
    documentoPersona: number;
    telefonoPersona: number;
}
