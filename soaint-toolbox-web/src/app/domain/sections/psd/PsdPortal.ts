export interface PsdPortal {

    valorLiquidacion: number;
    saldoFavor: number;
    salgoPendiente: number;
    interesMora: number;
    valorTotal: number;
    periodoCancelar: string;
    diasMora: number;
}
