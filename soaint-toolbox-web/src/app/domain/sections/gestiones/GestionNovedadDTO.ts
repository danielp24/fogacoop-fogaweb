export interface GestionNovedadDTO {

    decisionCoop: string;
    fechaCompromisoPago: Date;
    desicionJuntaDirectiva: string;
    tiempoEspera: number;
    requEstadosFinancieros: boolean;
}
