export interface OperacionRealizadaDTO {

    operacionRealizada: string;
    fechaOperacion: Date;
    observaciones: string;
}
