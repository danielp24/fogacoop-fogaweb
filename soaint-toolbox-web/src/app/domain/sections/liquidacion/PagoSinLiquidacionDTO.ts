export interface PagoSinLiquidacionDTO {

    fechaPagoL: Date;
    valorPagado: number;
}
