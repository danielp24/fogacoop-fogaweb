import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {PagesModule} from './ui/components/pages.module';
import {PRIMENG_MODULES} from './shared/primeNg/primeNg';
import {CooperativaInformationService} from './infraestructure/services/tables/cooperativa-information.service';
import { MessageService } from 'primeng/api';

/* */
@NgModule({
  declarations: [
    AppComponent
],
  imports: [
    PagesModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ...PRIMENG_MODULES,
  ],
  providers: [
    CooperativaInformationService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
