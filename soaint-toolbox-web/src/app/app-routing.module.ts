import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HeaderComponent} from './ui/components/layouts/header/header.component';
import { ConfirmarAbonoDevolucionComponent } from './ui/components/pages/confirmar-abono-devolucion/confirmar-abono-devolucion.component';
import { GestionarPagosPSDComponent } from './ui/components/pages/gestionar-pagos-psd/gestionar-pagos-psd.component';
import { ConsultaMovimientosCooperativaComponent } from './ui/components/pages/consulta-movimientos-cooperativa/consulta-movimientos-cooperativa.component';
import { ConsultaMovimientosCooperativaFogacoopComponent } from './ui/components/pages/consulta-movimientos-cooperativa-fogacoop/consulta-movimientos-cooperativa-fogacoop.component';
import { ConfirmacionPagoComponent } from './ui/components/pages/confirmacion-pago/confirmacion-pago.component';
import { LoginComponent } from './ui/components/pages/login/login.component';
import { InfoPagoComponent } from './ui/components/pages/info-pago/info-pago.component';
import { RegistrarpagosConsignacionComponent } from './ui/components/pages/registrarpagos-consignacion/registrarpagos-consignacion.component';
import { InfoPagoNoLiquidadoComponent } from './ui/components/pages/info-pago-no-liquidado/info-pago-no-liquidado.component';
import { RecuperarPassComponent } from './ui/components/pages/recuperar-pass/recuperar-pass.component';
import { NuevaContrasenaComponent } from './ui/components/pages/nueva-contrasena/nueva-contrasena.component';
import { MsjPasswordComponent } from './ui/components/pages/msj-password/msj-password.component';
import { TablaFechaPrimasComponent } from './ui/components/tools/tables/tabla-fecha-primas/tabla-fecha-primas.component';
import { ActualizacionDatosCooperativasComponent } from './ui/components/pages/actualizacion-datos-cooperativas/actualizacion-datos-cooperativas.component';

const routes: Routes = [
  {path: 'header', component: HeaderComponent},
  {path: 'actualizacionDatosCooperativas', component: ActualizacionDatosCooperativasComponent},
  {path: 'abonoDevolucionPago', component: ConfirmarAbonoDevolucionComponent},
  {path: 'gPagosPSD', component: GestionarPagosPSDComponent},
  {path: 'consultaMovimientosCooperativaFogacoop', component: ConsultaMovimientosCooperativaFogacoopComponent},
  {path: 'consultaMovimientosCooperativa', component: ConsultaMovimientosCooperativaComponent},
  {path: 'confirmacionPago', component: ConfirmacionPagoComponent},
  {path: 'login', component: LoginComponent},
  {path: 'infoPago', component: InfoPagoComponent},
  {path: 'rPagoConsignacion', component: RegistrarpagosConsignacionComponent},
  {path: 'infoPagoNoLiquidado', component: InfoPagoNoLiquidadoComponent},
  {path: 'recuperarPass', component: RecuperarPassComponent},
  {path: 'nuevaPass', component: NuevaContrasenaComponent},
  {path: 'msjPass', component: MsjPasswordComponent},
  {path: 'fechatabla', component: TablaFechaPrimasComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
