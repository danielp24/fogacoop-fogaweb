import { Injectable } from '@angular/core';
import { AutocompleteCooDTO } from 'src/app/domain/tools/AutocompleteCooDTO';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InfoAutocompletCoopService {
private info: AutocompleteCooDTO[] = [ {
  siaf: 123,
  nombre: 'Coopemtol',
  nit: 12345,
  tipoCooperativa: 'Cooperativa de trabajo'

}
];
  constructor(private http: HttpClient) {}
    getAutocompleteCoo() {
      return this.info;
    }
   }
