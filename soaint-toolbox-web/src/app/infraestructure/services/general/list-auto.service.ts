import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListAutoService {

  constructor(private http: HttpClient ) { }

  getList() {
      return this.http.get<any>('assets/demo/data/operaciones.json')
      .toPromise()
      // tslint:disable-next-line:no-angle-bracket-type-assertion
      .then(res => <any[]> res.data)
      .then(data => data);
  }
}
