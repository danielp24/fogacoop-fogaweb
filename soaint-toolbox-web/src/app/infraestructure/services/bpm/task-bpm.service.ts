import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {Observable} from "rxjs";
import {ProcessRequestDTO} from "../../../domain/bpm/request/ProcessRequestDTO";
import {ProcessResponseDTO} from "../../../domain/bpm/response/ProcessResponseDTO";
import {ApiManageService} from "../../api/api-manage.service";
import {RequestParamsDTO} from "../../../domain/bpm/request/RequestParamsDTO";

@Injectable({
  providedIn: 'root'
})
export class TaskBpmService {

  constructor( private _api: ApiManageService) { }

  consultarTareasPorInstancia (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
    const endpoint = environment.tarea_endPoint + '/tasksByProcessInstance';
    return this._api.post(endpoint, request);
  }

  consultarTareasPorGrupos (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
    const endpoint = environment.tarea_endPoint + '/tasksByGroups';
    return this._api.post(endpoint, request);
  }

  cambiarEstadoTarea (request: RequestParamsDTO) : Observable<ProcessResponseDTO> {
    const endpoint = environment.tarea_endPoint + '/taskStatus';
    return this._api.put(endpoint, request);
  }

  reasignarTarea (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
    const endpoint = environment.tarea_endPoint + '/reassignTask';
    return this._api.put(endpoint, request);
  }
}
