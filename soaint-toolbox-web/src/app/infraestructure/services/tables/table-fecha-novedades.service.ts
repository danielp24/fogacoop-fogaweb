import { Injectable } from '@angular/core';
import { TablaFechaNovedadDTO } from '../../../domain/sections/tables/tablaFechaNovedadDTO';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TableFechaNovedadesService {

  private tablaNovedad: TablaFechaNovedadDTO[] = [

    {
      fechaInicial: "04/04/18",
      fechaFinal: "04/05/18",
      tipoNovedad: "Alertas",
      valorNovedad: 20000
    },
    {
      fechaInicial: "14/12/18",
      fechaFinal: "21/05/21",
      tipoNovedad: "No Pago",
      valorNovedad: 10000
    },
  ];
  constructor(private http: HttpClient) { }

  getTableNovedad() {
    return this.tablaNovedad;
  }
}
