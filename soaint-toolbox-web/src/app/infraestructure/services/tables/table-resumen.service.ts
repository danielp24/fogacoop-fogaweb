import { Injectable } from '@angular/core';
import { TablaResumenDTO } from '../../../domain/sections/tables/tablaResumenDTO';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TableResumenService {
  private tableResumen: TablaResumenDTO[] = [
    {
      valorTotalPrima: 54654,
      valorPrimaNormal: 14,
      valorPosicionNeta: 4454654,
      valorSobrePrima: 54545,
      valorMora: 4444
    },
      {
      valorTotalPrima: 1111,
      valorPrimaNormal: 222,
      valorPosicionNeta: 3333,
      valorSobrePrima: 4,
      valorMora: 555
    }
    ];
  constructor(private http: HttpClient) { }

  getTableResumen() {
    return this.tableResumen;
  }
}
