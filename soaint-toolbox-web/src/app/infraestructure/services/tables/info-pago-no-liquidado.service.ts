import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PagoNoLiquidadoDTO } from '../../../domain/pages/PagoNoLiquidadoDTO';

@Injectable({
  providedIn: 'root'
})
export class InfoPagoNoLiquidadoService {

  private dataPagoNoLiquidado: PagoNoLiquidadoDTO[] = [
    {
      NIT: 'Holaa1-22',
      cooperativa: 'coopertativa',
      fechaPago: '16-06-2018',
      valorPagado: 50000,
      estado: 'activo',
      observaciones: 'azulz azul azul azul',
    },
    {
      NIT: 'Holaa2-22',
      cooperativa: 'coopertativa',
      fechaPago: '16-06-2014',
      valorPagado: 100000,
      estado: 'vencido',
      observaciones: 'azulz azul azul azul',
    }
  ];

  constructor( private http: HttpClient) {}

  getinfoPagoNoLiquidado() {
    return this.dataPagoNoLiquidado;
 }

}
