import { Injectable } from '@angular/core';
import {CooperativeInfoDTO} from '../../../domain/tools/CooperativeInfoDTO';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class CooperativaInformationService {

  private dataCoop: CooperativeInfoDTO[] = [{
    name: 'Foopep',
    charge: 'Gerente',
    email: 'foppep@foopep.coom',
    departamento: 'Bogotá D.C',
    city: 'Girardot',
    indicative: 1,
    conmutador: 266666,
    extension: 3004275989,
    fijo: 2455785,
    celular: 31222222,
    fecha: "16-06-2018"
  },
    {
      name: 'Confenalco',
      charge: 'Administrador',
      email: 'confenalco@confenalco.com',
      departamento: 'Tolima',
      city: 'Ibague',
      indicative: 2,
      conmutador: 975545245,
      extension: 354525785,
      fijo: 24232323 ,
      celular: 39992222,
      fecha: "16-06-2018"
    }
  ];
  constructor( private http: HttpClient) { }

  getInfoCooperative() {
    return this.dataCoop;
  }

}
