import { Injectable } from '@angular/core';
import { TablaFechaPagoDTO } from '../../../domain/sections/tables/tablaFechaPagoDTO';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TableInfoPagoService {

  private tableFechaPago: TablaFechaPagoDTO[] = [
    {
      fecha: "22/03/19",
      valorPagado: 5555
    },
    {
      fecha: "12/11/19",
      valorPagado: 11231421
    }
  ];
  constructor(private http: HttpClient) { }

  getTableFechaPago() {
    return this.tableFechaPago;
  }
}
