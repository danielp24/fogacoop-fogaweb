import { Component, OnInit } from '@angular/core';
import { Data } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-consulta-movimientos-cooperativa-fogacoop',
  templateUrl: './consulta-movimientos-cooperativa-fogacoop.component.html',
  styleUrls: ['./consulta-movimientos-cooperativa-fogacoop.component.css']
})
export class ConsultaMovimientosCooperativaFogacoopComponent implements OnInit {
  value: Data;
  buscar: any = {
    NIT: '',
    sigla: '',
    codigosiaf: ''
  };

  constructor() { }
  ngOnInit() {
  }

  Busqueda(forma: NgForm) {
    console.log(this.buscar);
  }
}
