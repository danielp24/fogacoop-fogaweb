import { Component, OnInit } from '@angular/core';
import { PagoNoLiquidadoDTO } from '../../../../domain/pages/PagoNoLiquidadoDTO';
import { InfoPagoNoLiquidadoService } from '../../../../infraestructure/services/tables/info-pago-no-liquidado.service';

@Component({
  selector: 'app-info-pago-no-liquidado',
  templateUrl: './info-pago-no-liquidado.component.html',
  styleUrls: ['./info-pago-no-liquidado.component.css']
})
export class InfoPagoNoLiquidadoComponent implements OnInit {

  pagoNoLiquidado: PagoNoLiquidadoDTO[];
  NoLiq: any[];
  infoNoPago: any[];

  constructor( private infoPagoNoLiquidado: InfoPagoNoLiquidadoService) { }

 ngOnInit() {

  this.infoNoPago = this.infoPagoNoLiquidado.getinfoPagoNoLiquidado();
  console.log(this.infoNoPago);

  this.NoLiq = [
    {field: 'NIT', header: 'NIT'},
    {field: 'cooperativa', header: 'Cooperativa'},
    {field: 'fechaPago', header: 'Fecha de pago'},
    {field: 'valorPagado', header: 'Valor Pagado'},
    {field: 'estado', header: 'Estado'},
    {field: 'observaciones', header: 'Observaciones'},
   ];
  }
}
