import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { ConsignacionDTO } from 'src/app/domain/sections/pagos/ConsignacionDTO';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-registrarpagos-consignacion',
  templateUrl: './registrarpagos-consignacion.component.html',
  styleUrls: ['./registrarpagos-consignacion.component.css']
})
export class RegistrarpagosConsignacionComponent implements OnInit {

  pagoConsignacion: ConsignacionDTO = {
    ciudad: '',
    fechaConsignacion: null,
    banco: [],
    numTransferencia: null,
    NIT: '',
    nomCooperativa: '',
    valorPagado: null,
    observaciones: ''
  };
  value: Date;
  form: FormGroup;

  constructor( private messageService: MessageService) { }

  ngOnInit() {
  }

  Guardar(forma: NgForm) {

    if ( this.pagoConsignacion.NIT === "" || this.pagoConsignacion.nomCooperativa === "" || this.pagoConsignacion.banco.length <= 0 || this.pagoConsignacion.numTransferencia <= 0 || this.pagoConsignacion.valorPagado <= 0 || this.pagoConsignacion.ciudad === "") {
       this.messageService.add({severity: 'error', summary: 'Error ', detail: 'Ingrese los datos correctamente.'});
    } else {
      console.log( this.pagoConsignacion);
    }

  }

}
