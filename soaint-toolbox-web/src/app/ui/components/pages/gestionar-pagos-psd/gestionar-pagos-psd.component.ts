import { Component, OnInit } from '@angular/core';
import { LiquidacionPsdDTO } from 'src/app/domain/sections/tables/LiquidacionPsdDTO';
import { LiquidacionPSDService } from 'src/app/infraestructure/services/tables/liquidacion-PSD.service';
import { GestionarPagosPSDService } from '../../../../infraestructure/services/tables/gestionar-pagos-psd.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {NameValuePairDTO} from "../../../../domain/bpm/general/NameValuePairDTO";
interface Options {
  name: string;
  code: string;
}
@Component({
  selector: 'app-gestionar-pagos-psd',
  templateUrl: './gestionar-pagos-psd.component.html',
  styleUrls: ['./gestionar-pagos-psd.component.css']
})
export class GestionarPagosPSDComponent implements OnInit {
  listaTipoIdent: NameValuePairDTO[];
  value: Date;
  dataLiquidacion: LiquidacionPsdDTO[] = [];
  liq: any[];
  infPag: any [];
  gestion: any[];
  pagosPSDT: any[];
  parameters: any;
  opcionesCooperativa: Options[];
  selectedCooperativa: Options;
  parametrosNegocio: any;

  constructor(private gestionPagoPSD: LiquidacionPSDService,
              private pagosPSD: GestionarPagosPSDService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.pagosPSDT = this.pagosPSD.getpagosPSDT();
    console.log(this.pagosPSDT);

    this.getParameters();

    this.infPag = [
      {field: 'InteresMora', header: 'Fecha'},
      {field: 'valDep', header: 'Valor Pagado'},
    ];

    this.gestion = [
      {field: 'fecha', header: 'Fecha'},
      {field: 'hora', header: 'Hora'},
    ];

    this.liq = [
      {field: 'pInputText' , header: 'Nombre Documento'},
    ];

    this.opcionesCooperativa = [
      {name: 'Retransmitir', code: 'Retransmitir'},
      {name: 'Devolucion/Abono', code: 'DevolucionAbono'},
      {name: 'Cancelar Inscripcion', code: 'CancelarInscripcion'},
      {name: 'Esperar Pago', code: 'EsperarPago'},
    ];
  }

  getParameters(){
    this.activatedRoute.queryParams.subscribe(params => {
      this.parameters = JSON.parse(atob(params['param']));
      //console.log('parameter',atob(this.parameters)); // Print the parameter to the console. 
    });

    //Consumiendo el containerId.
    console.log('containerId = ',this.parameters.containerId)
  }

  cambiarDecision(){
    console.log('Llego',this.selectedCooperativa.name);
    this.parametrosNegocio = {gestionSeguir:this.selectedCooperativa.code};
  }

}
