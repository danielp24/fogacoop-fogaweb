import { Component, OnInit } from '@angular/core';
import { TableFechaPrimaService } from '../../../../infraestructure/services/tables/table-fecha-prima.service';
import { TablaFechaPrimaDTO } from '../../../../domain/sections/tables/tablaFechaPrimaDTO';
import { TablaFechaNovedadDTO } from 'src/app/domain/sections/tables/tablaFechaNovedadDTO';
import { TableFechaNovedadesService } from 'src/app/infraestructure/services/tables/table-fecha-novedades.service';

@Component({
  selector: 'app-actualizacion-datos-cooperativas',
  templateUrl: './actualizacion-datos-cooperativas.component.html',
  styleUrls: ['./actualizacion-datos-cooperativas.component.css']
})
export class ActualizacionDatosCooperativasComponent implements OnInit {

  tablaFechaPrima: TablaFechaPrimaDTO[] = [];
  fechPrima: any[];

  tablaNovedad: TablaFechaNovedadDTO[] = [];
  novedad: any[];

  constructor( private tableFechaPrimaService: TableFechaPrimaService,
               private tablaNovedadService: TableFechaNovedadesService) { }

  ngOnInit() {

    this.fechPrima = [
      {field: 'fechaInicio', header: 'Fecha Inicio'},
      {field: 'fechaFinal', header: 'Fecha Final'},
      {field: 'tipoPrima', header: 'Tipo de Sobre/Prima'},
      {field: 'porcentaje', header: 'Porcentaje'}
    ];

    this.tablaFechaPrima = this.tableFechaPrimaService.getTableFechaPrima();
    console.log(this.tablaFechaPrima);

    this.novedad = [
      {field: 'fechaInicial', header: 'Fecha Inicial'},
      {field: 'fechaFinal', header: 'Fecha Final'},
      {field: 'tipoNovedad', header: 'Tipo de Novedad'},
    ];

    this.tablaNovedad = this.tablaNovedadService.getTableNovedad();
    console.log(this.tablaNovedad);
  }

}
