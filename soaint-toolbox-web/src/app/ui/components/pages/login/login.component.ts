import { Component, OnInit } from '@angular/core';
import { LoginDTO } from 'src/app/domain/pages/LoginDTO';
import { NgForm, FormGroup } from '@angular/forms';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: LoginDTO  = {
    cooperativa: '',
    password: ''
  };

  form: FormGroup;

  constructor(private messageService: MessageService) {
  }

  ngOnInit() {
  }
  showResponse(event) {
    // Aqui iria la validacion para permitir el recapchat
  }

  Ingresar(form: NgForm) {

    if ( this.usuario.cooperativa === "" || this.usuario.password === "") {
        this.messageService.add({severity: 'error', summary: 'Error ', detail: 'Ingrese todos los datos.'});
    } else {
      console.log( this.usuario);
    }

  }

}
