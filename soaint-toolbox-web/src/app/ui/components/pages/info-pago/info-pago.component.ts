import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { PagoPsdPortalDTO } from 'src/app/domain/sections/psd/PagoPsdPortalDTO';
import { MessageService } from 'primeng/api';
import { MessageModule } from 'primeng/message';

@Component({
  selector: 'app-info-pago',
  templateUrl: './info-pago.component.html',
  styleUrls: ['./info-pago.component.css']
})
export class InfoPagoComponent implements OnInit {

  infopago: PagoPsdPortalDTO = {
    banco: [],
    nombreTitular: '',
    tipoPersona: '',
    documentoPersona: null,
    telefonoPersona: null
  };

  forma: FormGroup;

  constructor( private messageService: MessageService) { }
  ngOnInit() {
  }

  Pagar(forma: NgForm) {
    // console.log(this.infopago);
    if ( this.infopago.banco.length <= 0 || this.infopago.nombreTitular === "" || this.infopago.tipoPersona === "" || this.infopago.documentoPersona <= 0 || this.infopago.telefonoPersona <= 0) {
       this.messageService.add({severity: 'error', summary: 'Error ', detail: 'Ingrese todos los datos.'});
    } else {
      console.log( this.infopago);
    }
    }
}



