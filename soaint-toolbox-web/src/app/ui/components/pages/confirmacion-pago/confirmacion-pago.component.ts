import { Component, OnInit } from '@angular/core';
import { ConfirmacionPagoDTO } from '../../../../domain/sections/pagos/ConfirmacionPagoDTO';

@Component({
  selector: 'app-confirmacion-pago',
  templateUrl: './confirmacion-pago.component.html',
  styleUrls: ['./confirmacion-pago.component.css']
})
export class ConfirmacionPagoComponent implements OnInit {
  value: Date;
  confirmacionPago: ConfirmacionPagoDTO = {
    entidad: 'XXXXX',
    NIT: 'XXXXX',
    fechaPago: null,
    estadoPago: '',
    numTransaccion: null,
    banco: [],
    valorPagado: null,
    descripcion: 'Azul'
  };

  constructor() { }
  ngOnInit() {
  }
  Finalizar() {
    console.log(this.confirmacionPago);
  }
}
