import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PRIMENG_MODULES } from 'src/app/shared/primeNg/primeNg';
import { ConsultaMovimientosComponent } from './tools/consulta-movimientos/consulta-movimientos.component';
import { HeaderComponent } from './layouts/header/header.component';
import { BtnGuardarContinuarComponent } from './tools/btn-guardar-continuar/btn-guardar-continuar.component';
import { ConfirmarAbonoDevolucionComponent } from './pages/confirmar-abono-devolucion/confirmar-abono-devolucion.component';
import { GestionarPagosPSDComponent } from './pages/gestionar-pagos-psd/gestionar-pagos-psd.component';
import { ListAutocompletableComponent } from './tools/list-autocompletable/list-autocompletable.component';
import { InfoBasicaCoopComponent } from './tools/info-basica-coop/info-basica-coop.component';
import { AutocompleteComponent } from './tools/input-autocomplete/input-autocomplete.component';
import { ConsultaMovimientosCooperativaComponent } from './pages/consulta-movimientos-cooperativa/consulta-movimientos-cooperativa.component';
import { InformacionContactoComponent } from './tools/tables/informacion-contacto/informacion-contacto.component';
import { ConsultaMovimientosCooperativaFogacoopComponent } from './pages/consulta-movimientos-cooperativa-fogacoop/consulta-movimientos-cooperativa-fogacoop.component';
import { ConfirmacionPagoComponent } from './pages/confirmacion-pago/confirmacion-pago.component';
import { LoginComponent } from './pages/login/login.component';
import { InfoPagoComponent } from './pages/info-pago/info-pago.component';
import { RegistrarpagosConsignacionComponent } from './pages/registrarpagos-consignacion/registrarpagos-consignacion.component';
import { InfoPagoNoLiquidadoComponent } from './pages/info-pago-no-liquidado/info-pago-no-liquidado.component';
import { RecuperarPassComponent } from './pages/recuperar-pass/recuperar-pass.component';
import { NuevaContrasenaComponent } from './pages/nueva-contrasena/nueva-contrasena.component';
import { CalendarioComponent } from './tools/calendario/calendario.component';
import { MsjPasswordComponent } from './pages/msj-password/msj-password.component';
import { TablaCalculoPrimasComponent } from './tools/tables/tabla-calculo-primas/tabla-calculo-primas.component';
import { TablaFechaPrimasComponent } from './tools/tables/tabla-fecha-primas/tabla-fecha-primas.component';
import { TablaFechaNovedadesComponent } from './tools/tables/tabla-fecha-novedades/tabla-fecha-novedades.component';
import { TablaResumenComponent } from './tools/tables/tabla-resumen/tabla-resumen.component';
import { TablaInfoPagoComponent } from './tools/tables/tabla-info-pago/tabla-info-pago.component';
import { BtnConsultarDirectivosComponent } from './tools/btn-consultar-directivos/btn-consultar-directivos.component';
import { ActualizacionDatosCooperativasComponent } from './pages/actualizacion-datos-cooperativas/actualizacion-datos-cooperativas.component';

@NgModule({
  declarations: [
    ConsultaMovimientosComponent,
    HeaderComponent,
    BtnGuardarContinuarComponent,
    GestionarPagosPSDComponent,
    ListAutocompletableComponent,
    InfoBasicaCoopComponent,
    AutocompleteComponent,
    ConfirmarAbonoDevolucionComponent,
    ConsultaMovimientosCooperativaComponent,
    InformacionContactoComponent,
    ConsultaMovimientosCooperativaFogacoopComponent,
    ConfirmacionPagoComponent,
    LoginComponent,
    InfoPagoComponent,
    RegistrarpagosConsignacionComponent,
    RecuperarPassComponent,
    InfoPagoNoLiquidadoComponent,
    NuevaContrasenaComponent,
    CalendarioComponent,
    MsjPasswordComponent,
    TablaCalculoPrimasComponent,
    TablaFechaPrimasComponent,
    TablaFechaNovedadesComponent,
    TablaResumenComponent,
    TablaInfoPagoComponent,
    BtnConsultarDirectivosComponent,
    ActualizacionDatosCooperativasComponent
     ],
  imports: [
    CommonModule,
    ...PRIMENG_MODULES
  ],
  exports: [
    InformacionContactoComponent,
    ConsultaMovimientosComponent,
    HeaderComponent,
    BtnGuardarContinuarComponent,
    GestionarPagosPSDComponent,
    ListAutocompletableComponent,
    InfoBasicaCoopComponent,
    GestionarPagosPSDComponent,
    AutocompleteComponent,
    InformacionContactoComponent,
    ConfirmacionPagoComponent,
    LoginComponent,
    RegistrarpagosConsignacionComponent,
    RecuperarPassComponent,
    NuevaContrasenaComponent,
    CalendarioComponent,
    InfoPagoComponent,
    MsjPasswordComponent,
    ...PRIMENG_MODULES
   ]
})
export class PagesModule { }
