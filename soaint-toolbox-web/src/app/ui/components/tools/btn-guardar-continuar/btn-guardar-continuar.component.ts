import {Component, Input, OnInit} from '@angular/core';
import {RequestParamsDTO} from "../../../../domain/bpm/request/RequestParamsDTO";
import {TaskBpmService} from "../../../../infraestructure/services/bpm/task-bpm.service";
import {OwnerUserDTO} from "../../../../domain/bpm/general/OwnerUserDTO";
import { environment } from 'src/environments/environment';
import { successResponse, statesTaskActions } from 'src/environments/environment.variables';
import {NameValuePairDTO} from "../../../../domain/bpm/general/NameValuePairDTO";
import { ValuesDTO } from 'src/app/domain/bpm/request/ValuesDTO';

@Component({
  selector: 'app-btn-guardar-continuar',
  templateUrl: './btn-guardar-continuar.component.html',
  styleUrls: ['./btn-guardar-continuar.component.css']
})
export class BtnGuardarContinuarComponent implements OnInit {
  @Input() parametrosBPM: any;
  @Input() parametrosNegocio ?: any;
/*
  @Input() listaTipoIdent: NameValuePairDTO[];
*/
  rolSelect = new NameValuePairDTO();

  //@Output() guardarParametros : EventEmitter<TareaDTO> = new EventEmitter<TareaDTO>();

  constructor(private service: TaskBpmService) {

  }

  ngOnInit() {
    console.log("COMPONENT = "+this.parametrosBPM)
   /* this.listaTipoIdent.forEach(element => {
      if (element.name != null)
        this.rolSelect = element.name;

    });*/
  }

  receiveParameters() {
    if (this.parametrosBPM) {
      const callParameters = new RequestParamsDTO();
      callParameters.ownerUser = new OwnerUserDTO();
      callParameters.containerId = this.parametrosBPM.containerId;
      callParameters.taskId = this.parametrosBPM.taskId;
      callParameters.taskStatus = statesTaskActions.completed;
      callParameters.ownerUser.user = this.parametrosBPM.ownerUser.user;
      callParameters.ownerUser.password = this.parametrosBPM.ownerUser.password;
      callParameters.parametros = new ValuesDTO();
      if(this.parametrosNegocio != null){
        this.llenarParametrosDinamicos(callParameters);
      }
      console.log(JSON.stringify(callParameters))

      this.service.cambiarEstadoTarea(callParameters)
      .subscribe(data => {
         if (data.response.msg == successResponse.type) {
          this.finalizarTarea();
         }
      });
    }
  }


  finalizarTarea(){
    parent.postMessage( "redirect", environment.mainHost);
  }

  llenarParametrosDinamicos(parametrosDinamicos : RequestParamsDTO){
    console.log(this.parametrosNegocio);
    parametrosDinamicos.parametros.values = this.parametrosNegocio;
  }

}
