import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-btn-consultar-directivos',
  templateUrl: './btn-consultar-directivos.component.html',
  styleUrls: ['./btn-consultar-directivos.component.css']
})
export class BtnConsultarDirectivosComponent implements OnInit {
  display: boolean;
  constructor() { }

  ngOnInit() {
  }

  showDialog() {
    this.display = true;
}

}
