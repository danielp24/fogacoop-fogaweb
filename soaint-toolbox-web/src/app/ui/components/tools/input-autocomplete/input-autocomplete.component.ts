import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-input-autocomplete',
  templateUrl: './input-autocomplete.component.html',
  styleUrls: ['./input-autocomplete.component.css']
})
export class AutocompleteComponent {
  filteredCountriesSingle: any[];
  @Input() data: string;

  constructor(private http: HttpClient) { }

  filterCountrySingle(keyWord) {
    this.http.get("https://restcountries.eu/rest/v2/lang/es").subscribe(
      (response: any[]) => {
        console.log(response);
        this.filteredCountriesSingle = this.filterCountry(
          keyWord.query,
          response
        );
      },
      error => console.log("ERROR " + error)
    );
  }

  filterCountry(keyWord, countries: any[]): any[] {
    // tslint:disable-next-line:prefer-const
    let filtered: any[] = [];
    if (this.data === "name") {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < countries.length; i++) {
        // tslint:disable-next-line:prefer-const
        let paises = countries[i];
        if (paises.name.toLowerCase().indexOf(keyWord.toLowerCase()) === 0) {
          filtered.push(paises);
        }
      }
      return filtered;
    }
   /* if (this.data === "capital") {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < countries.length; i++) {
        // tslint:disable-next-line:prefer-const
        let paises = countries[i];
        if (paises.capital.toLowerCase().indexOf(keyWord.toLowerCase()) === 0) {
          filtered.push(paises);
        }
      }
      console.log(filtered);
      return filtered;
    }  */
  }
  }

