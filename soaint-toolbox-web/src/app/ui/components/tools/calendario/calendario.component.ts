import { Component, OnInit, } from '@angular/core';
import {CalendarModule, Calendar} from 'primeng/calendar';

@Component ({
   selector: 'app-calendario',
    templateUrl: './calendario.component.html'
})

 // tslint:disable-next-line:one-line
 export class CalendarioComponent implements OnInit{

    date1: Date;
    date2: Date;
    date3: Date;
    date4: Date;
    date5: Date;
    date6: Date;
    date7: Date;
    date8: Date;
    date9: Date;
    date10: Date;
    date11: Date;
    date12: Date;
    date13: Date;
    date14: Date;
    dates: Date[];
    rangeDates: Date[];
    minDate: Date;
    maxDate: Date;
    invalidDates: Array<Date>;
    es: any;
    icono: Calendar["showIcon"] = true;

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
      this.es = {
        firstDayOfWeek: 1,
        dayNames: [ "domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado" ],
        dayNamesShort: [ "dom", "lun", "mar", "mié", "jue", "vie", "sáb" ],
        dayNamesMin: [ "D", "L", "M", "X", "J", "V", "S" ],
          monthNames: [ "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" ],
        monthNamesShort: [ "ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic" ],
          today: 'Hoy',
          clear: 'Borrar'
    };
                          // tslint:disable-next-line:prefer-const
      let today = new Date();
      // tslint:disable-next-line:prefer-const
      let month = today.getMonth();
      // tslint:disable-next-line:prefer-const
      let year = today.getFullYear();
      // tslint:disable-next-line:prefer-const
      let prevMonth = (month === 0) ? 11 : month - 1;
      // tslint:disable-next-line:prefer-const
      let prevYear = (prevMonth === 11) ? year - 1 : year;
      // tslint:disable-next-line:prefer-const
      let nextMonth = (month === 11) ? 0 : month + 1;
      // tslint:disable-next-line:prefer-const
      let nextYear = (nextMonth === 0) ? year + 1 : year;
      this.minDate = new Date();
      this.minDate.setMonth(prevMonth);
      this.minDate.setFullYear(prevYear);
      this.maxDate = new Date();
      this.maxDate.setMonth(nextMonth);
      this.maxDate.setFullYear(nextYear);
      // tslint:disable-next-line:prefer-const
      let invalidDate = new Date();
      invalidDate.setDate(today.getDate() - 1);
      this.invalidDates = [today, invalidDate];
  }
 }
// tslint:disable-next-line:eofline
