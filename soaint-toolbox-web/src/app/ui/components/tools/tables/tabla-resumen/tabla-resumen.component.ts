import { Component, OnInit } from '@angular/core';
import { TablaResumenDTO } from 'src/app/domain/sections/tables/tablaResumenDTO';
import { TableResumenService } from 'src/app/infraestructure/services/tables/table-resumen.service';

@Component({
  selector: 'app-tabla-resumen',
  templateUrl: './tabla-resumen.component.html',
  styleUrls: ['./tabla-resumen.component.css']
})
export class TablaResumenComponent implements OnInit {

  tablaResumen: TablaResumenDTO[] = [];
  resum: any[];
  constructor(private tableResumenService: TableResumenService) { }

  ngOnInit() {

    this.resum = [
      { field: 'valorTotalPrima', header: 'Valor Total Prima' },
      { field: 'valorPrimaNormal', header: 'Valor Prima Normal' },
      { field: 'valorPosicionNeta', header: 'Valor Posición Neta' },
      { field: 'valorSobrePrima', header: 'Valor Sobreprima' },
      { field: 'valorMora', header: 'Valor Mora' }
    ];

    this.tablaResumen = this.tableResumenService.getTableResumen();
    console.log(this.tablaResumen);
  }

}
