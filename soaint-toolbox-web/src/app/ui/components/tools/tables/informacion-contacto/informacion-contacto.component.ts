import { Component, OnInit } from '@angular/core';
import { CooperativeInfoDTO } from 'src/app/domain/tools/CooperativeInfoDTO';
import { CooperativaInformationService } from 'src/app/infraestructure/services/tables/cooperativa-information.service';

@Component({
  selector: 'app-informacion-contacto',
  templateUrl: './informacion-contacto.component.html',
  styleUrls: ['./informacion-contacto.component.css']
})



export class InformacionContactoComponent implements OnInit {
  dataCoop: CooperativeInfoDTO[] = [];
  cols: any[];
  constructor(private infoCooperativa: CooperativaInformationService) { }

  ngOnInit() {
    this.dataCoop = this.infoCooperativa.getInfoCooperative();
    console.log(this.dataCoop);

    this.cols = [
      { field: 'name', header: 'Nombre' },
      { field: 'charge', header: 'Cargo' },
      { field: 'email', header: 'Correo Electrónico' },
      { field: 'departamento', header: 'Departamento' },
      { field: 'city', header: 'Ciudad' },
      { field: 'indicative', header: 'Indicativo' },
      { field: 'conmutador', header: 'Conmutador' },
      { field: 'extension', header: 'Extensión' },
      { field: 'fijo', header: 'Fijo' },
      { field: 'celular', header: 'Celular' }
    ];
  }

}

