import { Component, OnInit } from '@angular/core';
import { TablaFechaNovedadDTO } from '../../../../../domain/sections/tables/tablaFechaNovedadDTO';
import { TableFechaNovedadesService } from '../../../../../infraestructure/services/tables/table-fecha-novedades.service';

@Component({
  selector: 'app-tabla-fecha-novedades',
  templateUrl: './tabla-fecha-novedades.component.html',
  styleUrls: ['./tabla-fecha-novedades.component.css']
})
export class TablaFechaNovedadesComponent implements OnInit {

  tablaNovedad: TablaFechaNovedadDTO[] = [];
  novedad: any[];
  constructor(private tablaNovedadService: TableFechaNovedadesService) { }

  ngOnInit() {

    this.novedad = [
      {field: 'fechaInicial', header: 'Fecha Inicial'},
      {field: 'fechaFinal', header: 'Fecha Final'},
      {field: 'tipoNovedad', header: 'Tipo de Novedad'},
      {field: 'valorNovedad', header: 'Valor Novedad'},
    ];

    this.tablaNovedad = this.tablaNovedadService.getTableNovedad();
    console.log(this.tablaNovedad);
  }

}
