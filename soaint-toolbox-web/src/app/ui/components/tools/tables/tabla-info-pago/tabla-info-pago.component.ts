import { Component, OnInit } from '@angular/core';
import { TablaFechaPagoDTO } from '../../../../../domain/sections/tables/tablaFechaPagoDTO';
import { TableInfoPagoService } from '../../../../../infraestructure/services/tables/table-fecha-pago.service';

@Component({
  selector: 'app-tabla-info-pago',
  templateUrl: './tabla-info-pago.component.html',
  styleUrls: ['./tabla-info-pago.component.css']
})
export class TablaInfoPagoComponent implements OnInit {

  tablaFechaPago: TablaFechaPagoDTO[] = [];
  fechPago: any[];
  constructor(private tableFechaPagoService: TableInfoPagoService) { }

  ngOnInit() {

    this.fechPago = [
      {field: 'fecha', header: 'Fecha de Pago'},
      {field: 'valorPagado', header: 'Valor Pagado'}
    ];

    this.tablaFechaPago = this.tableFechaPagoService.getTableFechaPago();
    console.log(this.tablaFechaPago);
  }

}
