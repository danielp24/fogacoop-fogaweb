import { Component, OnInit, Input, Output } from '@angular/core';
import { ListAutoService } from 'src/app/infraestructure/services/general/list-auto.service';


@Component({
  selector: 'app-list-autocompletable',
  templateUrl: './list-autocompletable.component.html',
  styleUrls: ['./list-autocompletable.component.css']
})
export class ListAutocompletableComponent implements OnInit {


operation: any;
filteredOperations: any[];

constructor(private listService: ListAutoService) {

}
ngOnInit() {
  }
  filteredOperation(event) {
    const query = event.query;
    this.listService.getList().then(operations => {
      this.filteredOperations = this.searchOperation(query, operations);
    });
  }

  searchOperation(query, operations: any[]): any[] {
    const filtered: any[] = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < operations.length; i++) {
      const operation = operations[i];
      if (operation.name.toLowerCase().indexOf(query.toLowerCase()) === 0) {
          filtered.push(operation);
      }
    }
    return filtered;
  }


}
