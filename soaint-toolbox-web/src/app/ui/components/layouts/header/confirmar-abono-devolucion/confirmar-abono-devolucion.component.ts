import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-confirmar-abono-devolucion',
  templateUrl: './confirmar-abono-devolucion.component.html',
  styleUrls: ['./confirmar-abono-devolucion.component.css']
})
export class ConfirmarAbonoDevolucionComponent implements OnInit {

  value: Date;
  parameters : any;

constructor(private router: Router,
            private activatedRoute: ActivatedRoute){}

  ngOnInit() {
    this.getParameters();
  }

  getParameters(){
    this.activatedRoute.queryParams.subscribe(params => {
      this.parameters = JSON.parse(atob(params['param']));
      //console.log('parameter',atob(this.parameters)); // Print the parameter to the console. 
    });

    //Consumiendo el containerId.
    console.log('containerId = ',this.parameters.containerId)
  }

}
