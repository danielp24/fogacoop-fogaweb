const hostApiBpm = 'http://192.168.1.52:8090/api';
const mainHost = 'http://localhost:4200';


export const environment = {
  production: false,
// Api BPM
  contenedor_endPoint: `${hostApiBpm}/container`,
  proceso_endPoint: `${hostApiBpm}/process`,
  tarea_endPoint: `${hostApiBpm}/task`,
  regla_endPoint: `${hostApiBpm}/rule`,
  mainHost : `${mainHost}`,
};
